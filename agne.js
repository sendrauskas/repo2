document.addEventListener('DOMContentLoaded', function() {

    var dropdownElems = document.querySelector('.dropdown-trigger');
    var dropdownInstances = M.Dropdown.init(dropdownElems);
    var carouselElems = document.querySelector('.carousel.carousel-slider');
    var carouselInstance = M.Carousel.init(carouselElems, {
            fullWidth: true,
            indicators: true
        });
  });

function moveNextCarousel() {
        var elems = document.querySelector('.carousel.carousel-slider');
        var moveRight = M.Carousel.getInstance(elems);
        moveRight.next(1);
    };
function movePrevCarousel() {
        var elems = document.querySelector('.carousel.carousel-slider');
        var moveLeft = M.Carousel.getInstance(elems);
        moveLeft.prev(1);
    };

function myFunctionNone() {
  document.getElementById("myDIV").style.display = "none";
  document.getElementById("vardas").value ="";
  document.getElementById("emailas").value ="";
  document.getElementById("large_text").value ="";
    };

function myFunctionInline2() {
  document.getElementById("myDIV").style.display = "inline";
    };

function getCardGina() {
    var p_name = document.getElementById('vardas').value;
    var p_email = document.getElementById('emailas').value;
    var content = document.getElementById('large_text').value;
    var card = '<p>'+p_name+', Thanks for message!</p>';
    M.toast({html: p_name+', Thanks for message!'})
    // document.getElementById('card-row').innerHTML = card;
    myFunctionNone();
    };
