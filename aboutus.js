document.addEventListener('DOMContentLoaded', function() {
    var dropdownElems = document.querySelector('.dropdown-trigger');
    var dropdownInstances = M.Dropdown.init(dropdownElems);
    var carouselElems = document.querySelector('.carousel.carousel-slider');
    var carouselInstance = M.Carousel.init(carouselElems, {
            fullWidth: true,
            indicators: true,
            onCycleTo: function(x) {
                // console.log(x.id);
                var klase=x.id;
                var biuro_adresas="";
                var biuro_nuoroda="";
                var biuro_maps="";
                switch(klase) 
                    {
                 case "biuras2":
                    biuro_adresas="Laisvės al.76, Kaunas"
                    biuro_nuoroda="https://www.kaunas.lt";
                    document.getElementById('map_keitimas').innerHTML ='<div class="gmap_canvas"><iframe style="width: 100%; height: 262px;" id="gmap_canvas" src="https://maps.google.com/maps?q=Lithuania%20Kaunas%20Laisv%C4%97s%20al.%2076&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style></style>';
                    break;
                case "biuras3":
                    biuro_adresas="Žvejų g.22, LT-91241 Klaipėda"
                    biuro_nuoroda="https://www.klaipeda.lt";
                    document.getElementById('map_keitimas').innerHTML ='<div class="gmap_canvas"><iframe style="width: 100%; height: 262px;" id="gmap_canvas" src="https://maps.google.com/maps?q=Lithuania%20Klaip%C4%97da%20%C5%BDvej%C5%B3%2022&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style></style>'
                    break;
                default:
                    biuro_adresas="Subačiaus g.2, LT-01127 Vilnius"
                    biuro_nuoroda="https://www.vilnius.lt";
                    document.getElementById('map_keitimas').innerHTML ='<div class="gmap_canvas"><iframe style="width: 100%; height: 262px;" id="gmap_canvas" src="https://maps.google.com/maps?q=Lithuania%20Vilnius%20Suba%C4%8Diaus%202&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style></style>';
                   };
                var card ='<div class="col s12"><div class="card"><div class="card-image"><img src="pict/'+klase+'in.jpg"><span class="card-title"></span></div><div class="card-content"><p>Adress:<br>'+biuro_adresas+'</p></div><div class="card-action"><a href="'+biuro_nuoroda+'" target="_blank">City Link</a></div></div></div>';
                document.getElementById('adreso_keitimas').innerHTML = card;
            }
        });
  });

function moveNextCarousel() {
        var elems = document.querySelector('.carousel.carousel-slider');
        var moveRight = M.Carousel.getInstance(elems);
        moveRight.next(1);
    };
function movePrevCarousel() {
        var elems = document.querySelector('.carousel.carousel-slider');
        var moveLeft = M.Carousel.getInstance(elems);
        moveLeft.prev(1);
    };

